# Progetto applicazione per collegamento a 2 siti

## Finalita'
L'applicazione si basa sull'utilizzo di activity e di intent impliciti

## Funzionamento
Una volta avviata l'app essa presenta 2 bottoni posizionati al centro del layout dell'activity main.

weblinks e' il nome dell'applicazione, essa deve presentare nel layout un colore grigio chiaro o azzurro chiaro.
weblinks deve aver al di sopra dei bottoni del layout il testo seguente "scegli il sito"

Premendo sul primo bottone [Sito1] l'applicazione deve collegarsi al sito www.itiszuccante.gov.it.
Premendo sul secondo bottone [Sito2] l'applicazione deve collegarsi al sito www.didatticamente.net.
Su entrambi i bottoni va inserito un messaggio a fumetto indicante il collegamento a ciascun sito.

## Procedura
Avviare android studio: Start new Android Studio project > Empty Activity. Per realizzare l'app saranno necessari i widget **Button** e **TextView**.  
 
Si creino 2 bottoni sul layout dell'activity principale. 
Si fornisce del codice per raggiungere l'obiettivo 
relativo agli Intent impliciti

```
public void onClick(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("geo:45.495403,12.257509"));
        startActivity(i);
    }
```

Sono evidenti **action** e **data** si rimanda alla documentazione: [qui](https://developer.android.com/reference/android/content/Intent.html). 
Si noti che i dati vanno impostati come URI (Uniform Resource Identifier).


NB. L'argomento "geo:45.495403,12.257509" e' una stringa interpretabile solo se nello smartphone 
e' stata installata l'applicazione google maps. 
